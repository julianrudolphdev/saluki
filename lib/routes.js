Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function(){
    this.route('home', {
        path: '/',
        template: 'home'
    });
    
    //menu routes
    this.route('menu', {
        path: '/menu',
        template: 'menu'
    });
    
    //admin routes
    //admin views
  this.route('login', {
    path: '/admin',
    template: 'login'
  });

  this.route('items', {
    path: '/admin/items',
    template: 'items',
    data: function(){
      templateData = {
        items: Items.find()
      };
      return templateData;
    },
    onBeforeAction: function(){
      if(!Meteor.userId() || Meteor.userId() === null){
        Router.go('/');
      }
      this.next();
    }
  });

  this.route('add_item', {
    path: '/admin/item/add',
    template: 'add_item',
    data: function(){
      templateData = {
        categories: Categories.find()
      };
      return templateData;
    },
    onBeforeAction: function(){
      if(!Meteor.userId() || Meteor.userId() === null){
        Router.go('/');
      }
      this.next();
    }
  });

  this.route('edit_item', {
    path: '/admin/item/:_id/edit',
    template: 'edit_item',
    data: function(){
      var currentItem = this.params._id;
      return Items.findOne({_id: currentItem});
      templateData = {
        categories: Categories.find()
      };
      return templateData;
    },
    onBeforeAction: function(){
      if(!Meteor.userId() || Meteor.userId() === null){
        Router.go('/');
      }
      this.next();
    }
  });
  
    //categories routes
  this.route('category_items',{
    path: '/categories/:slug',
    template: 'category_items',
    data: function(){
      templateData = {
        category_items: Items.find({category: this.params.slug})
      };
      return templateData;
    }
  });
  
});