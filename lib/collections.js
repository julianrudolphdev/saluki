//categories Collection
Categories = new Mongo.Collection('categories');

//items Collection
Items = new Mongo.Collection('items');

Items.attachSchema(new SimpleSchema({
  name: {
    type: String,
    max: 100
  },
  is_featured:{
    type: String,
    max: 1,
    optional: true
  },
  category:{
    type: String,
  },
  description: {
    type: String,
    max: 500
  },
  userId: {
    type: String,
    autoValue: function() {return Meteor.userId()}
  },
  createdAt:{
    type: Date,
    autoValue: function(){return new Date()}
  },
  ItemImage: {
    type: String,
    max: 100,
    optional: true
  }
}));


//item img collection
ItemImages = new FS.Collection('ItemImages', {
  stores: [new FS.Store.GridFS('ItemImages')]
});
