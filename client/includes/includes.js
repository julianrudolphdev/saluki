document.title="Saluki Bar 'n' Grill";


Template.login.events({
  'submit .login_user': function(event){
    event.preventDefault();
    var email = event.target.email.value;
    var password = event.target.password.value;

    Meteor.loginWithPassword(email, password, function(err){
      if(err){
        event.target.email.value = email;
        event.target.password.value = password;
        FlashMessages.sendError(err.reason);
      }else{
        FlashMessages.sendSuccess("You are now logged in!");
        Router.go('/admin/items');
      }
    });
    event.target.email.value = '';
    event.target.password.value = '';
  }
});

Template.layout.events({
  'click .logout_user': function(event){
    event.preventDefault();
    Meteor.logout(function(err){
      if(err){
        FlashMessages.sendError(err.reason);
      }else{
        FlashMessages.sendSuccess("You are now logged out!");
        Router.go('/');
      }
    });
  }
});