Template.add_item.events({
  'submit .add_category': function(event){
    event.preventDefault();
    var name = event.target.name.value;
    var slug = event.target.slug.value;
    Categories.insert({
      name: name,
      slug: slug
    });
    event.target.name.value = '';
    event.target.slug.value = '';
    FlashMessages.sendSuccess("Category Added!");
  }
});

Template.add_item.events({
  'submit .add_item': function(event){
    event.preventDefault();

    var name = event.target.name.value;
    var category = event.target.category.value;
    var description = event.target.description.value;
    var is_featured = event.target.is_featured.value;
    //grab img

    var file = $('#itemImage').get(0).files[0];
    //check if img
    if(file){
      fsFile = new FS.File(file);

      //insert file into Collection
      ItemImages.insert(fsFile, function(err, result){
        if(!err){
          var ItemImage = '/cfs/files/ItemImages/'  +  result._id;

          //insert into items Collection
          Items.insert({
            name: name,
            category: category,
            description: description,
            is_featured: is_featured,
            ItemImage: ItemImage,
            createdAt: new Date()
          });
        }
      });
    }else{
      //insert with no img

      Items.insert({
        name: name,
        category: category,
        description: description,
        is_featured: is_featured,
        createdAt: new Date()
      });
    }
    //reset form
    event.target.name.value = '';
    event.target.category.value = '';
    event.target.description.value = '';
    event.target.is_featured.value = '';

    //send message
    FlashMessages.sendSuccess("Item Added");
    Router.go('/admin/items');

  }
});

Template.edit_item.events({
  'submit .edit_item': function(event){
    event.preventDefault();

    var name = event.target.name.value;
    var category = event.target.category.value;
    var description = event.target.description.value;
    var is_featured = event.target.is_featured.value;
    //grab img

    var file = $('#itemImage').get(0).files[0];
    //check if img
    if(file){
      fsFile = new FS.File(file);

      //insert file into Collection
      ItemImages.insert(fsFile, function(err, result){
        if(!err){
          var ItemImage = '/cfs/files/ItemImages/'  +  result._id;

          //update item
          Items.update({
            _id:this._id
          },{
            $set:{
              name: name,
              category: category,
              decription: description,
              is_featured: is_featured,
              ItemImage: ItemImage,
              updatedAt: new Date()
            }
          });

        }
      });
    }else{

      //update item no img
      Items.update({
        _id:this._id
      },{
        $set:{
          name: name,
          category: category,
          decription: description,
          is_featured: is_featured,
          updatedAt: new Date()
        }
      });
    }
    //reset form
    event.target.name.value = '';
    event.target.category.value = '';
    event.target.description.value = '';
    event.target.is_featured.value = '';

    //send message
    FlashMessages.sendSuccess("Item Added");
    Router.go('/admin/items');

  }
});



Template.items.events({
  'click .delete_item': function(event){
    event.preventDefault();
    var currentItem = this._id;
    if(confirm("Delete Item?")){
      Items.remove(currentItem);
    }
    FlashMessages.sendSuccess("Item Deleted!");
    Router.go('/admin/items');
  }
});

Template.add_item.events({
  'click .delete_cat': function(event){
    event.preventDefault();
    var currentCat = this._id;
    if(confirm("Delete Category?")){
      Categories.remove(currentCat);
    }
    FlashMessages.sendSuccess("Category Deleted!");
  }
});
